require 'test_helper'

class WarzoneplayersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @warzoneplayer = warzoneplayers(:one)
  end

  test "should get index" do
    get warzoneplayers_url
    assert_response :success
  end

  test "should get new" do
    get new_warzoneplayer_url
    assert_response :success
  end

  #test "should create warzoneplayer" do
  #  assert_difference('Warzoneplayer.count') do
  #    post warzoneplayers_url, params: { warzoneplayer: { livello: @warzoneplayer.livello, nickname: @warzoneplayer.nickname } }
  #  end

  #  assert_redirected_to warzoneplayer_url(Warzoneplayer.last)
  #end

  test "should show warzoneplayer" do
    get warzoneplayer_url(@warzoneplayer)
    assert_response :success
  end

  test "should get edit" do
    get edit_warzoneplayer_url(@warzoneplayer)
    assert_response :success
  end

  #test "should update warzoneplayer" do
  #  patch warzoneplayer_url(@warzoneplayer), params: { warzoneplayer: { livello: @warzoneplayer.livello, nickname: @warzoneplayer.nickname } }
  #  assert_redirected_to warzoneplayer_url(@warzoneplayer)
  #end

  #test "should destroy warzoneplayer" do
  # assert_difference('Warzoneplayer.count', -1) do
  #    delete warzoneplayer_url(@warzoneplayer)
  #  end

  #  assert_redirected_to warzoneplayers_url
  #end
end
