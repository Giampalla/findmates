require 'test_helper'

class LolplayersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @lolplayer = lolplayers(:one)
  end

  test "should get index" do
    get lolplayers_url
    assert_response :success
  end

  test "should get new" do
    get new_lolplayer_url
    assert_response :success
  end

  #test "should create lolplayer" do
  #  assert_difference('Lolplayer.count') do
  #    post lolplayers_url, params: { lolplayer: { elo: @lolplayer.elo, nickname: @lolplayer.nickname, ruolo: @lolplayer.ruolo } }
  #  end

  #  assert_redirected_to lolplayer_url(Lolplayer.last)
  #end

  test "should show lolplayer" do
    get lolplayer_url(@lolplayer)
    assert_response :success
  end

  test "should get edit" do
    get edit_lolplayer_url(@lolplayer)
    assert_response :success
  end

  #test "should update lolplayer" do
  #  patch lolplayer_url(@lolplayer), params: { lolplayer: { elo: @lolplayer.elo, nickname: @lolplayer.nickname, ruolo: @lolplayer.ruolo} }
  #  assert_redirected_to lolplayer_url(@lolplayer)
  #end

  #test "should destroy lolplayer" do
  #  assert_difference('Lolplayer.count', -1) do
  #    delete lolplayer_url(@lolplayer)
  #  end

  #  assert_redirected_to lolplayers_url
  #end
end
