require 'test_helper'

class ValorantplayersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @valorantplayer = valorantplayers(:one)
  end

  test "should get index" do
    get valorantplayers_url
    assert_response :success
  end

  test "should get new" do
    get new_valorantplayer_url
    assert_response :success
  end

  #test "should create valorantplayer" do
  #  assert_difference('Valorantplayer.count') do
  #    post valorantplayers_url, params: { valorantplayer: { elo: @valorantplayer.elo, nickname: @valorantplayer.nickname, ruolo: @valorantplayer.ruolo } }
  #  end

  #  assert_redirected_to valorantplayer_url(Valorantplayer.last)
  #end

  test "should show valorantplayer" do
    get valorantplayer_url(@valorantplayer)
    assert_response :success
  end

  test "should get edit" do
    get edit_valorantplayer_url(@valorantplayer)
    assert_response :success
  end

  #test "should update valorantplayer" do
  #  patch valorantplayer_url(@valorantplayer), params: { valorantplayer: { elo: @valorantplayer.elo, nickname: @valorantplayer.nickname, ruolo: @valorantplayer.ruolo } }
  #  assert_redirected_to valorantplayer_url(@valorantplayer)
  #end

  #test "should destroy valorantplayer" do
  #  assert_difference('Valorantplayer.count', -1) do
  #    delete valorantplayer_url(@valorantplayer)
  #  end

  #  assert_redirected_to valorantplayers_url
  #end
end
