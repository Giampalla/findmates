require "application_system_test_case"

class LolplayersTest < ApplicationSystemTestCase
  setup do
    @lolplayer = lolplayers(:one)
  end

  test "visiting the index" do
    visit lolplayers_url
    assert_selector "h1", text: "Lolplayers"
  end

  test "creating a Lolplayer" do
    visit lolplayers_url
    click_on "New Lolplayer"

    fill_in "Elo", with: @lolplayer.elo
    fill_in "Nickname", with: @lolplayer.nickname
    fill_in "Ruolo", with: @lolplayer.ruolo
    click_on "Create Lolplayer"

    assert_text "Lolplayer was successfully created"
    click_on "Back"
  end

  test "updating a Lolplayer" do
    visit lolplayers_url
    click_on "Edit", match: :first

    fill_in "Elo", with: @lolplayer.elo
    fill_in "Nickname", with: @lolplayer.nickname
    fill_in "Ruolo", with: @lolplayer.ruolo
    click_on "Update Lolplayer"

    assert_text "Lolplayer was successfully updated"
    click_on "Back"
  end

  test "destroying a Lolplayer" do
    visit lolplayers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Lolplayer was successfully destroyed"
  end
end
