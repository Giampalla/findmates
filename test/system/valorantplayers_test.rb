require "application_system_test_case"

class ValorantplayersTest < ApplicationSystemTestCase
  setup do
    @valorantplayer = valorantplayers(:one)
  end

  test "visiting the index" do
    visit valorantplayers_url
    assert_selector "h1", text: "Valorantplayers"
  end

  test "creating a Valorantplayer" do
    visit valorantplayers_url
    click_on "New Valorantplayer"

    fill_in "Elo", with: @valorantplayer.elo
    fill_in "Nickname", with: @valorantplayer.nickname
    fill_in "Ruolo", with: @valorantplayer.ruolo
    click_on "Create Valorantplayer"

    assert_text "Valorantplayer was successfully created"
    click_on "Back"
  end

  test "updating a Valorantplayer" do
    visit valorantplayers_url
    click_on "Edit", match: :first

    fill_in "Elo", with: @valorantplayer.elo
    fill_in "Nickname", with: @valorantplayer.nickname
    fill_in "Ruolo", with: @valorantplayer.ruolo
    click_on "Update Valorantplayer"

    assert_text "Valorantplayer was successfully updated"
    click_on "Back"
  end

  test "destroying a Valorantplayer" do
    visit valorantplayers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Valorantplayer was successfully destroyed"
  end
end
