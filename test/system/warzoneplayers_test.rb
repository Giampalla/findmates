require "application_system_test_case"

class WarzoneplayersTest < ApplicationSystemTestCase
  setup do
    @warzoneplayer = warzoneplayers(:one)
  end

  test "visiting the index" do
    visit warzoneplayers_url
    assert_selector "h1", text: "Warzoneplayers"
  end

  test "creating a Warzoneplayer" do
    visit warzoneplayers_url
    click_on "New Warzoneplayer"

    fill_in "Livello", with: @warzoneplayer.livello
    fill_in "Nickname", with: @warzoneplayer.nickname
    click_on "Create Warzoneplayer"

    assert_text "Warzoneplayer was successfully created"
    click_on "Back"
  end

  test "updating a Warzoneplayer" do
    visit warzoneplayers_url
    click_on "Edit", match: :first

    fill_in "Livello", with: @warzoneplayer.livello
    fill_in "Nickname", with: @warzoneplayer.nickname
    click_on "Update Warzoneplayer"

    assert_text "Warzoneplayer was successfully updated"
    click_on "Back"
  end

  test "destroying a Warzoneplayer" do
    visit warzoneplayers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Warzoneplayer was successfully destroyed"
  end
end
