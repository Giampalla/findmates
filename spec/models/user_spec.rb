 #   t.string "email", default: "", null: false
 #   t.string "encrypted_password", default: "", null: false
 #   t.string "reset_password_token"
 #   t.datetime "reset_password_sent_at"
 #   t.datetime "remember_created_at"
 #   t.datetime "created_at", precision: 6, null: false
 #   t.datetime "updated_at", precision: 6, null: false
 #   t.string "provider", limit: 50, default: "", null: false
 #   t.string "uid", limit: 500, default: "", null: false
 #   t.boolean "admin"
 #   t.index ["email"], name: "index_users_on_email", unique: true
 #   t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true



require 'rails_helper'

RSpec.describe User, type: :model do
  context 'test di correzione' do

    it 'controllo presenza email'do
       user = User.new(password: "password123").save
       expect(user).to eq(false)
    end

    it 'controllo presenza password'do
       user = User.new(email: 'email1@exemple.com').save
       expect(user).to eq(false)
    end

    it 'controllo salvataggio'do
       user = User.new(email: 'email1@exemple.com',password: "password123").save
       expect(user).to eq(true)
    end

  end
end
