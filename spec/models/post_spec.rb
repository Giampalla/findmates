require 'rails_helper'

RSpec.describe Post, type: :model do
  context 'test di correzione' do
  current_user = User.first_or_create!(email: 'prova@exmple.com',password: 'password',password_confirmation: 'password') 
  
  it 'controllo presenza user_id'do
   post= Post.new(body: 'body valido',views: 0,gioco: 'valorant')
   expect(post).to_not be_valid    
  end


   it 'controllo presenza testo'do
    post= Post.new(views: 0,gioco: 'valorant',user_id: 1)
    expect(post).to_not be_valid  
  end

   it 'controllo views intere'do
    post= Post.new(body: 'body valido',views: 0,gioco: 'valorant',user_id: current_user.id)
    expect(post.views).to be_a(Integer)  
  end
  end
end
