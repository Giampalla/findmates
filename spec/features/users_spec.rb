require 'rails_helper'

RSpec.feature "Users", type: :feature do
    
      

      scenario "registrazione con dati validi" do

        visit "/"

        click_link "Registrati"
        expect(current_path).to eq(new_user_registration_path)

        fill_in "email", with: "tester@example.com"
        fill_in "password", with: "test-password"
        fill_in "password confirmation", with: "test-password"
        click_button "Sign up"

        expect(current_path).to eq "/"
        expect(page).to have_content("Welcome! You have signed up successfully.")
      end

   

    context "Rgistrazione con dati non validi" do

     	before do
         visit new_user_registration_path
        end

       scenario "Form vuota" do
         #expect_fields_to_be_blank
         click_button "Sign up"
         expect(page).to have_content("3 errors prohibited this user from being saved:")
       end
      
        scenario "Password diverse" do
			    fill_in "Email", with: "tester@example.com"
      		fill_in "Password", with: "test-password"
      		fill_in "Password confirmation", with: "test-password2"
      		click_button "Sign up"
          expect(page).to have_content("Password confirmation doesn't match Password")
    	end

    	scenario "Email non valida" do
			    fill_in "Email", with: "invalid-email"
      	  fill_in "Password", with: "test-password"
     		  fill_in "Password confirmation", with: "test-password"
      		click_button "Sign up"
          expect(page).to have_content("Email is invalid")
      end

       scenario "Password troppo corta" do
          min_password_length = 6
          too_short_password = "p" * (min_password_length - 1)
          fill_in "Email", with: "someone@example.tld"
          fill_in "Password", with: too_short_password
          fill_in "Password confirmation", with: too_short_password
          click_button "Sign up"
          expect(page).to have_content("Password is too short (minimum is 6 characters)")
      end

    end




    #def expect_fields_to_be_blank
    #	expect(page).to have_field("Email", with: "", type: "email")

    #	expect(find_field("Password", type: "password").value).to be_nil
    #	expect(find_field("Password confirmation", type: "password").value).to be_nil
    #end


end
