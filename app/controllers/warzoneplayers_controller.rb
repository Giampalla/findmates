class WarzoneplayersController < ApplicationController
  before_action :set_warzoneplayer, only: [:show, :edit, :update, :destroy]
  before_action :is_admin!, only: [:destroy]
  before_action :authenticate_user!
  # GET /warzoneplayers
  # GET /warzoneplayers.json
  def index
    @warzoneplayers = Warzoneplayer.all
  end

  # GET /warzoneplayers/1
  # GET /warzoneplayers/1.json
  def show
  end

  # GET /warzoneplayers/new
  def new
    @warzoneplayer = Warzoneplayer.new
  end

  # GET /warzoneplayers/1/edit
  def edit
  end

  # POST /warzoneplayers
  # POST /warzoneplayers.json
  def create
    @warzoneplayer = Warzoneplayer.new(warzoneplayer_params)
    @warzoneplayer.user_id = current_user.id

    respond_to do |format|
      if @warzoneplayer.save
        format.html { redirect_to home_profile_path, notice: 'Warzoneplayer was successfully created.' }
        format.json { render :show, status: :created, location: @warzoneplayer }
      else
        format.html { render :new }
        format.json { render json: @warzoneplayer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /warzoneplayers/1
  # PATCH/PUT /warzoneplayers/1.json
  def update
    respond_to do |format|
      if @warzoneplayer.update(warzoneplayer_params)
        format.html { redirect_to home_profile_path, notice: 'Warzoneplayer was successfully updated.' }
        format.json { render :show, status: :ok, location: @warzoneplayer }
      else
        format.html { render :edit }
        format.json { render json: @warzoneplayer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warzoneplayers/1
  # DELETE /warzoneplayers/1.json
  def destroy
    @warzoneplayer.destroy
    respond_to do |format|
      format.html { redirect_to warzoneplayers_url, notice: 'Warzoneplayer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warzoneplayer
      @warzoneplayer = Warzoneplayer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def warzoneplayer_params
      params.require(:warzoneplayer).permit(:nickname, :livello)
    end
end
