class HomeController < ApplicationController
  def index
  end
  
  def about
  end

  def profile
	@lolplayers= Lolplayer.find_by(user_id: current_user.id)
	@valorantplayers= Valorantplayer.find_by(user_id: current_user.id)
	@warzoneplayers= Warzoneplayer.find_by(user_id: current_user.id)  	
  end

end
