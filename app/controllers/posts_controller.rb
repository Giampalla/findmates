class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all.order("created_at DESC").paginate(page: params[:page])
  end

  def lolposts
    @posts = Post.where(gioco: "LEAGUE OF LEGENDS").order("created_at DESC").paginate(page: params[:page])
  end

  def valorantposts
    @posts = Post.where(gioco: "VALORANT").order("created_at DESC").paginate(page: params[:page])
  end

  def warzoneposts
    @posts = Post.where(gioco: "WARZONE").order("created_at DESC").paginate(page: params[:page])
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    views = @post.views + 1
    @post.update(views: views)

    @lolplayers= Lolplayer.find_by(user_id: current_user.id)
    @valorantplayers= Valorantplayer.find_by(user_id: current_user.id)
    @warzoneplayers= Warzoneplayer.find_by(user_id: current_user.id)
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @lolplayers= Lolplayer.find_by(user_id: current_user.id)
    @valorantplayers= Valorantplayer.find_by(user_id: current_user.id)
    @warzoneplayers= Warzoneplayer.find_by(user_id: current_user.id)

    @post = Post.new(post_params)
    @post.user = current_user

    
    gioco = @post.gioco
    f = 0

    if gioco == 'LEAGUE OF LEGENDS' && !@lolplayers
      redirect_to new_lolplayer_path
    elsif gioco == 'VALORANT' && !@valorantplayers
      redirect_to new_valorantplayer_path
    elsif gioco == 'WARZONE' && !@warzoneplayers
      redirect_to new_warzoneplayer_path
    elsif gioco == 'LEAGUE OF LEGENDS' 
      @post.titolo = @lolplayers.nickname + " | Ruolo " + @lolplayers.ruolo + " | Elo " + @lolplayers.elo
      f = 1
    elsif gioco == 'VALORANT'
      @post.titolo = @valorantplayers.nickname + " | Ruolo " + @valorantplayers.ruolo + " | Elo " + @valorantplayers.elo
      f = 1
    elsif gioco == 'WARZONE'
      @post.titolo = @warzoneplayers.nickname + " | Livello " + @warzoneplayers.livello.to_s
      f = 1
    end

    


    if f == 1  
      respond_to do |format|
        if @post.save
            format.html { redirect_to @post, notice: 'Post was successfully created.' }
            format.json { render :show, status: :created, location: @post }
        else
          format.html { render :new }
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    end
    

    
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:gioco, :body)
    end
end
