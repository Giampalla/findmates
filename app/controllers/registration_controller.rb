class RegistrationController  < Devise::RegistrationsController
    def editfoto
        @user = current_user
        if @user
            render :editfoto
        else
            render file: 'public/404', status: 404, formats: [:html]
         end 
    end
 end