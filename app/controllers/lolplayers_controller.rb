class LolplayersController < ApplicationController
  before_action :set_lolplayer, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :is_admin!, only: [:destroy]

  # GET /lolplayers
  # GET /lolplayers.json
  def index
    @lolplayers = Lolplayer.all
  end

  # GET /lolplayers/1
  # GET /lolplayers/1.json
  def show
  end

  # GET /lolplayers/new
  def new
    @lolplayer = Lolplayer.new
  end

  # GET /lolplayers/1/edit
  def edit
  end

  # POST /lolplayers
  # POST /lolplayers.json
  def create
    @lolplayer = Lolplayer.new(lolplayer_params)
    @lolplayer.user_id = current_user.id

    respond_to do |format|
      if @lolplayer.save
        format.html { redirect_to home_profile_path, notice: 'Lolplayer was successfully created.' }
        format.json { render :show, status: :created, location: @lolplayer }
        
      else
        format.html { render :new }
        format.json { render json: @lolplayer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lolplayers/1
  # PATCH/PUT /lolplayers/1.json
  def update
    respond_to do |format|
      if @lolplayer.update(lolplayer_params)
        format.html { redirect_to home_profile_path, notice: 'Lolplayer was successfully updated.' }
        format.json { render :show, status: :ok, location: @lolplayer }
      else
        format.html { render :edit }
        format.json { render json: @lolplayer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lolplayers/1
  # DELETE /lolplayers/1.json
  def destroy
    @lolplayer.destroy
    respond_to do |format|
      format.html { redirect_to lolplayers_url, notice: 'Lolplayer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lolplayer
      @lolplayer = Lolplayer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def lolplayer_params
      params.require(:lolplayer).permit(:nickname, :elo, :ruolo)
    end
end
