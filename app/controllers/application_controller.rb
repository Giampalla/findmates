class ApplicationController < ActionController::Base
	helper_method :is_admin!
    
    before_action :configure_permitted_parameters,
      if: :devise_controller?

     protected
     def configure_permitted_parameters
      	devise_parameter_sanitizer.permit(:sign_up, keys: [:avatar])
      	devise_parameter_sanitizer.permit(:account_update, keys: [:avatar])
     end

	private

	def is_admin!
		if current_user && current_user.admin?
		else
			redirect_to root_path
		end
	end

end
