class OmniauthController < ApplicationController

	def google_oauth2
		@user= User.create_from_provider_data(request.env['omniauth.auth'])
		if @user.persisted?
			sign_in_and_redirect @user
		else
			flash[:error] = "Errore con l'accesso con Google, Per favore registrati o prova ad accedere di nuovo."
			redirect_to new_user_registration_url
		end
	end


	def failure
		flash[:error] = "C'è stato un problema con il Log in. Registrati o prova ad accedere di nuovo."
		redirect_to new_user_registration_url
	end

end
