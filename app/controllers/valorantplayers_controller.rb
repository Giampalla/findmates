class ValorantplayersController < ApplicationController
  before_action :set_valorantplayer, only: [:show, :edit, :update, :destroy]
  before_action :is_admin!, only: [:destroy]
  before_action :authenticate_user!
  # GET /valorantplayers
  # GET /valorantplayers.json
  def index
    @valorantplayers = Valorantplayer.all
  end

  # GET /valorantplayers/1
  # GET /valorantplayers/1.json
  def show
  end

  # GET /valorantplayers/new
  def new
    @valorantplayer = Valorantplayer.new
  end

  # GET /valorantplayers/1/edit
  def edit
  end

  # POST /valorantplayers
  # POST /valorantplayers.json
  def create
    @valorantplayer = Valorantplayer.new(valorantplayer_params)
    @valorantplayer.user_id = current_user.id
    respond_to do |format|
      if @valorantplayer.save
        format.html { redirect_to home_profile_path, notice: 'Valorantplayer was successfully created.' }
        format.json { render :show, status: :created, location: @valorantplayer }
      else
        format.html { render :new }
        format.json { render json: @valorantplayer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /valorantplayers/1
  # PATCH/PUT /valorantplayers/1.json
  def update
    respond_to do |format|
      if @valorantplayer.update(valorantplayer_params)
        format.html { redirect_to home_profile_path, notice: 'Valorantplayer was successfully updated.' }
        format.json { render :show, status: :ok, location: @valorantplayer }
      else
        format.html { render :edit }
        format.json { render json: @valorantplayer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /valorantplayers/1
  # DELETE /valorantplayers/1.json
  def destroy
    @valorantplayer.destroy
    respond_to do |format|
      format.html { redirect_to valorantplayers_url, notice: 'Valorantplayer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_valorantplayer
      @valorantplayer = Valorantplayer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def valorantplayer_params
      params.require(:valorantplayer).permit(:nickname, :elo, :ruolo)
    end
end
