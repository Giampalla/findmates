class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2]
  
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  
  has_one_attached :avatar
  after_commit :add_default_avatar, on: %i[create update]

  

  def self.create_from_provider_data(provider_data)
  	where(provider: provider_data.provider, uid: provider_data.uid).first_or_create do |user|
  		user.email = provider_data.info.email
  	    user.password = Devise.friendly_token[0, 20]
  	end
  end

         has_one :lolplayer, dependent: :destroy
         has_one :valorantplayer, dependent: :destroy
         has_one :warzoneplayer, dependent: :destroy
         has_many :posts, dependent: :destroy
         
  def username
    return email.split('@')[0].capitalize
  end


  
  def avatar_thumbnail
    if avatar.attached?
     avatar.variant(resize: "150x150!").processed
    else
     "/default_profile.jpeg"
    end
  end

  private
   def add_default_avatar
    unless avatar.attached?
      avatar.attach(
        io: File.open(
          Rails.root.join(
            'app','assets','images','default_profile.jpeg'
          )
        ),
        filename: 'default_profile.jpeg',
        content_type: 'image/jpeg'
      )
   end
  end
end
