class Lolplayer < ApplicationRecord
	belongs_to :user
   
    validates :nickname, presence: true, length: {minimum: 5}
end
