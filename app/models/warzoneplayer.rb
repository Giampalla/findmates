class Warzoneplayer < ApplicationRecord
	belongs_to :user

	validates :nickname,presence: true, length: {minimum: 5}
	validates :livello , numericality: { less_than: 155,  only_integer: true }, presence: true
end
