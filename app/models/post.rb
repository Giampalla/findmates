class Post < ApplicationRecord
  belongs_to :user

  has_rich_text :body

  validates :body, presence: true, length: {minimum: 5}

  self.per_page = 3

end
