json.extract! valorantplayer, :id, :nickname, :elo, :ruolo, :created_at, :updated_at
json.url valorantplayer_url(valorantplayer, format: :json)
