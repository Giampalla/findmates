Rails.application.routes.draw do
  
  resources :posts do 
    get 'lolposts' , on: :collection
    get 'valorantposts' , on: :collection
    get 'warzoneposts' , on: :collection
  end

  resources :warzoneplayers
  resources :valorantplayers
  resources :lolplayers
  devise_for :users, controllers: {omniauth_callbacks: 'omniauth'}
  
  devise_scope :user do 
     match 'users/editfoto' => 'registration#editfoto', :via => [:get], :as => 'editfoto'
  end


  root 'home#index'
  get 'home/about'
  get 'home/profile'

  scope "admin" do
    get "users", to: "users#index"
    get "users/:id", to: "users#show", as: "user"
    delete "users/:id", to: "users#destroy"
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
