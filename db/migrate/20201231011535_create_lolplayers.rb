class CreateLolplayers < ActiveRecord::Migration[6.0]
  def change
    create_table :lolplayers do |t|
      t.string :nickname
      t.string :elo
      t.string :ruolo

      t.timestamps
    end
  end
end
