class CreateWarzoneplayers < ActiveRecord::Migration[6.0]
  def change
    create_table :warzoneplayers do |t|
      t.string :nickname
      t.integer :livello

      t.timestamps
    end
  end
end
