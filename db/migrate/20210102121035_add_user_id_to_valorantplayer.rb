class AddUserIdToValorantplayer < ActiveRecord::Migration[6.0]
  def change
    add_column :valorantplayers, :user_id, :integer
    add_index :valorantplayers, :user_id
  end
end
