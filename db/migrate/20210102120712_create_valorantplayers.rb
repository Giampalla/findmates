class CreateValorantplayers < ActiveRecord::Migration[6.0]
  def change
    create_table :valorantplayers do |t|
      t.string :nickname
      t.string :elo
      t.string :ruolo

      t.timestamps
    end
  end
end
