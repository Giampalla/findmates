class AddUserIdToWarzoneplayer < ActiveRecord::Migration[6.0]
  def change
    add_column :warzoneplayers, :user_id, :integer
    add_index :warzoneplayers, :user_id
  end
end
