class AddUserIdToLolplayer < ActiveRecord::Migration[6.0]
  def change
    add_column :lolplayers, :user_id, :integer
    add_index :lolplayers, :user_id
  end
end
